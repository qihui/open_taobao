require 'active_support/core_ext'
require 'digest'
require 'yaml'
require 'json'
require 'faraday'
require 'recursive_open_struct'

module OpenTaobao
  API_VERSION = '2.0'

  class << self
    attr_accessor :config

    def load(config_file)
      @config = YAML.load_file(config_file)
      @config = config[Rails.env] if defined? Rails
    end

    def connection
      conn = Faraday.new(config['endpoint'])
      conn.build
      conn
    end

    def sign(params)
      Digest::MD5::hexdigest("#{config['secret_key']}#{sorted_option_string params}#{config['secret_key']}").upcase
    end

    def sorted_option_string(options)
      options.map {|k, v| "#{k}#{v}" }.sort.join
    end

    def full_options(params)
      {
          :timestamp   => Time.now.strftime('%F %T'),
          :v           => API_VERSION,
          :format      => :json,
          :sign_method => :md5,
          :app_key     => config['app_key']
      }.merge params
    end

    def query_string(params)
      params = full_options params
      params[:sign] = sign params
      params.to_query
    end

    def url(params)
      '%s?%s' % [config['endpoint'], query_string(params)]
    end

    def parse_result(data)
      RecursiveOpenStruct.new(JSON.parse(data))
    end

    def get(params)
      path = url(params)
      parse_result Faraday.get(path).body
    end
  end
end
