# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'open_taobao/version'

Gem::Specification.new do |gem|
  gem.name          = 'open_taobao'
  gem.version       = OpenTaobao::VERSION
  gem.authors       = %w(GC)
  gem.email         = %w(guangcheng.song@gmail.com)
  gem.description   = %q{淘宝开放平台ruby版，支持Rails3}
  gem.summary       = %q{Open Taobao API for ruby}
  gem.homepage      = 'http://www.luckegg.com'

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = %w(lib)

  gem.add_dependency 'faraday'
  gem.add_dependency 'recursive-open-struct'
end
